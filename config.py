
class BaseConfig(object):
    DEBUG = False
    SECRET_KEY = "super secret key"
    DEMO = False

class DevelopmentConfig(object):
    DEBUG = True
    DEMO = False

class DemoConfig(object):
    DEBUG = True
    DEMO = True

class ProductionConfig(object):
    DEBUG = False
    DEMO = False
    SECRET_KEY = "TqJrRHIaNiD54kVmByX8"
