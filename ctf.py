from flask import Flask, render_template, make_response, request, jsonify
from urllib.request import urlopen
import os
import csv
import pycard

LEVELS = 25

app = Flask(__name__)
app.config.from_object(os.environ['APP_SETTINGS'])

@app.route('/')
def index():
    return render_template("site/index.html")

@app.route('/rules')
def rules():
    return render_template("site/rules.html", levels=LEVELS)

@app.route('/about')
def about():
    return render_template("site/about.html")

@app.route('/tools')
def tools():
    return render_template("site/tools.html")

@app.route('/404')
def p404():
    return render_template("site/404.html")

@app.route('/rankings')
def rankings():
    url = 'https://dl.dropboxusercontent.com/u/995442/score.csv'
    teams = []
    response = urlopen(url)
    csvfile = csv.reader(response.read().decode('utf-8').splitlines())
    next(csvfile)
    for row in csvfile:
        teams.append(row)
    teams.sort(key=lambda x: int(x[1]), reverse=True)
    return render_template("site/rankings.html", teams=teams)

@app.route('/teams')
def teams():
    url = 'https://dl.dropboxusercontent.com/u/995442/teams.csv'
    teams = []
    response = urlopen(url)
    csvfile = csv.reader(response.read().decode('utf-8').splitlines())
    next(csvfile)
    for row in csvfile:
        teams.append(row)
    return render_template("site/teams.html", teams=teams)

@app.route('/challenges')
def challenges():
    scores = []
    url = 'https://dl.dropboxusercontent.com/u/995442/challenges.csv'
    response = urlopen(url)
    csvfile = csv.reader(response.read().decode('utf-8').splitlines())
    next(csvfile)
    for row in csvfile:
        scores.append(row)
    if app.config['DEMO']:
        return render_template("site/index.html")
    else:
        return render_template("site/challenges.html", scores=scores)

@app.route('/levelone')
def levelone():
    return  render_template("levels/l1.html", level=1)

@app.route('/leveltwo')
def leveltwo():
    return render_template("levels/l2.html", level=2)

@app.route('/levelthree')
def levelthree():
    return render_template("levels/l3.html", level=3)

@app.route('/levelfour')
def levelfour():
    resp = make_response(render_template("levels/l4.html", level=4))
    resp.set_cookie("flag","the_flag_is_hungry", path='/levelfour')
    return resp

@app.route('/levelfive')
def levelfive():
    return render_template("levels/l5.html", level=5)

@app.route('/levelsix')
def levelsix():
    return render_template("levels/l6.html", level=6)

@app.route('/levelseven')
def levelseven():
    resp = make_response(render_template("levels/l7.html",  level=7), 200)
    resp.headers['The_flag_is'] = 'spyros'
    return resp

@app.route('/leveleight')
def leveleight():
    return render_template("levels/l8.html", level=8)

@app.route('/levelnine', methods=["GET", "POST"])
def levelenine():
    error = None
    if request.method == 'POST':
        if request.form['username'] != 'admin' or request.form['password'] != 'changeme':
            error = "Error: Invalid login"
        else:
            error = "Correct: the_flag_is_lego"
    return render_template("levels/l9.html", level=9, error=error)

@app.route('/levelten')
def levelten():
    return render_template("levels/l10.html", level=10)

@app.route('/leveleleven')
def leveleleven():
    return render_template("levels/l11.html", level=11)

@app.route('/leveltwelve')
def leveltwelve():
    return render_template("levels/l12.html", level=12)

@app.route('/levelthirteen')
def levelthirteen():
    return render_template("levels/l13.html", level=13)

@app.route('/levelthirteen.old')
def levelthirteenold():
    return render_template('levels/l13_flag.html'), 200, {'Content-Type': 'text/plain'}

@app.route('/levelfourteen')
def levelfourteen():
    return render_template("levels/l14.html", level=14)

@app.route('/levelfifteen', methods=["GET", "POST"])
def levelfifteen():
    error = None
    file = open('/tmp/the_flag_is_wolverine', 'w')
    file.close()
    if request.method == "POST":
        received = "host " + request.form['host']
        sanitized = str.replace(received,"rm","")
        sanitizedTmp = "cd /tmp;" + sanitized
        error = os.popen(sanitizedTmp).read()
    return render_template("levels/l15.html", level=15, error=error)

@app.route('/levelsixteen')
def levelsixteen():
    return render_template("levels/l16.html", level=16)

@app.route('/levelseventeen')
def levelseventeen():
    return render_template("levels/l17.html", level=17)

@app.route('/leveleighteen')
def leveleighteen():
    return render_template("levels/l18.html", level=18)

@app.route('/levelnineteen', methods=["GET", "POST"])
def levelnineteen():
    error = None
    if request.method == "POST":
        received = request.form['company']
        if received == "NetEnt":
            error = "the_flag_is_yeahbaby"
        else:
            error = "I told you the answer!"
    return render_template("levels/l19.html", level=19, error=error)

@app.route('/leveltwenty')
def leveltwenty():
    return render_template("levels/l20.html",level=20)

@app.route('/leveltwentyone')
def leveltwentyone():
    return render_template("levels/l21.html",level=21)

@app.route('/leveltwentytwo')
def leveltwentytwo():
    resp = make_response(render_template("levels/l22.html",level=22))
    secret = request.headers.get('Secret')
    if secret == "Innovation":
        resp.headers['flag'] = 'the_flag_is_awesome'
    return resp

@app.route('/leveltwentythree')
def leveltwentythree():
    return render_template("levels/l23.html", level=23)

@app.route('/robots.txt')
def robot():
    return render_template('site/robots.html'), 200, {'Content-Type': 'text/plain'}

@app.route('/leveltwentyfour')
def leveltwentyfour():
    agent = request.user_agent.platform.lower()
    if agent == "iphone":
        return render_template("levels/l24_flag.html", level=24)
    else:
        return render_template("levels/l24.html", level=24)

@app.route('/leveltwentyfive', methods=["GET", "POST"])
def leveltwentyfive():
    error = None
    if request.method == 'POST':
        cc = pycard.Card(
            number = request.form['cc'],
            month = 1,
            year = 2020,
            cvc = 123
        )
        if cc.is_valid:
            error = "the_flag_is_money"
        else:
            error = "Not a valid credit card"
    return render_template("levels/l25.html", level=25, error=error)

@app.route('/easteregg')
def easteregg():
    if "https" in request.url:
        xhralt = 1
    else:
        xhralt = 2
    resp = make_response(render_template("easteregg.html",level=100,xhralt=xhralt))
    return resp

@app.route('/easteregg_xhr')
def easteregg_xhr():
    origin = request.headers.get('Origin')
    if "netent.com" in origin:
        resp = make_response(jsonify(key="Here a cookie for you"))
        resp.set_cookie("flag",value="open_sesame",path="/1xasdg!/")
    else:
        resp = make_response(jsonify(key="Cookies are given only to netent.com origins"))
    resp.headers['Access-Control-Allow-Credentials']="true"
    resp.headers['Access-Control-Allow-Origin'] = origin
    return resp

@app.route('/1xasdg!/<id>')
def hiddenlevel(id):
    if 'flag' in request.cookies and request.cookies.get('flag') == 'open_sesame':
        resp = make_response(jsonify(key="Congratulations!. the_flag_is_superb"))
    else:
        resp = make_response(jsonify(key="You are missing something"))
    return resp

if __name__ == '__main__':
    app.run(host='0.0.0.0')
