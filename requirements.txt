cffi==1.5.2
dominate==2.1.17
Flask==0.10.1
Flask-Script==2.0.5
Flask-WTF==0.12
gunicorn==19.4.5
itsdangerous==0.24
Jinja2==2.8
Mako==1.0.3
MarkupSafe==0.23
passlib==1.6.5
git+https://github.com/orokusaki/pycard.git@master
pycparser==2.14
python-editor==0.5
six==1.10.0
visitor==0.1.2
Werkzeug==0.11.4
WTForms==2.1
